# keycloak-spring-boot

PoC: Resource server megvalósítás szereplör alapú jogosultság kezeléssel keycloak-kal.

## Funkciók

- Keycloak - Spring Boot resource server integráció, keycloak library nélkül
- OIDC felhasználó azonosítás
- szerepkör alapú jogosultságkezelés
- ki lehet kapcsolni a jogosultságkezelést `nosec` profil megadással
- minimális beállítás és megvalósítás: ami csak lehet alapértelmezés
