package org.example;

import javax.annotation.security.RolesAllowed;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@SecurityRequirement(name = "Keycloak")
public interface UserApi {

    @GetMapping(value = "/users", produces = APPLICATION_JSON_VALUE)
    UserList users();

    @GetMapping(value = "/player", produces = APPLICATION_JSON_VALUE)
    @RolesAllowed("player")
    UserList usersForPlayer();

    @GetMapping(value = "/public/user", produces = APPLICATION_JSON_VALUE)
    UserList usersForEverybody();

    @PostMapping(value = "/public/user", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    User postUserForEverybody(@RequestBody User user);

}
