package org.example;

import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController implements UserApi {

    @Override
    public UserList users() {
        return getUserList();
    }

    @Override
    public UserList usersForPlayer() {
        return getUserList();
    }

    @Override
    public UserList usersForEverybody() {
        return getUserList();
    }

    @Override
    public User postUserForEverybody(User user) {
        return user;
    }

    private static UserList getUserList() {
        SecurityContext context = SecurityContextHolder.getContext();
        Authentication authentication = context.getAuthentication();
        String name = authentication.getName();
        List<String> authorities = authentication.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .toList();
        return new UserList(List.of(new User(name, authorities)));
    }

}
