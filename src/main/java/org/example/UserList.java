package org.example;

import java.util.List;

import lombok.Value;

@Value
public class UserList {

    List<User> items;

}
